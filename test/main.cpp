#include <my/array.h>
#include <gtest/gtest.h>

#include <numeric>

TEST(array, size)
{
    my::array<int, 0> const a;
    my::array<int, 5> const b = {1, 2, 3, 4, 5};
    EXPECT_EQ(a.size(), 0u);
    EXPECT_EQ(b.size(), 5u);
    EXPECT_EQ(a.empty(), true);
    EXPECT_EQ(b.empty(), false);
}
TEST(array, accum)
{
    my::array<int, 6> const a = {1, 2, 3, 4, 5, 6};
    EXPECT_EQ(std::accumulate(a. begin(), a. end(), 0), 21);
    EXPECT_EQ(std::accumulate(a.rbegin(), a.rend(), 0), 21);
}
TEST(array, sort)
{
    my::array<int, 6> a = {1, 2, 3, 4, 5, 6};
    std::sort(a.begin(), a.end(), std::greater<int>{});
    EXPECT_EQ(a, (my::array<int, 6>{6, 5, 4, 3, 2, 1}));
}
TEST(array, bindings)
{
    my::array<int, 4> const a = {0, 1, 2, 3};
    auto const &[a0, a1, a2, a3] = a;
    EXPECT_EQ(a0, 0);
    EXPECT_EQ(a1, 1);
    EXPECT_EQ(a2, 2);
    EXPECT_EQ(a3, 3);
}
TEST(array, compare)
{
    my::array<int, 5> const less = {1, 2, 2, 4, 5};
    my::array<int, 5> const a    = {1, 2, 3, 4, 5};
    my::array<int, 5> const more = {1, 2, 3, 5, 5};

    auto const allcmp = []<typename T, std::size_t N>(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
        -> my::array<bool, 6>
    {
        return
        {
            lhs <  rhs,
            lhs <= rhs,
            lhs >  rhs,
            lhs >= rhs,
            lhs == rhs,
            lhs != rhs,
        };
    };
    EXPECT_EQ(allcmp(a,    a), (my::array<bool, 6>{false,  true, false,  true,  true, false}));
    EXPECT_EQ(allcmp(a, less), (my::array<bool, 6>{false, false,  true,  true, false,  true}));
    EXPECT_EQ(allcmp(a, more), (my::array<bool, 6>{ true,  true, false, false, false,  true}));
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
